#include <QCoreApplication>
#include <QDebug>

#include <KIO/ApplicationLauncherJob>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // Disable app quit lock during KIO jobs usage
    // > The application will quit when there are no more QEventLoopLockers operating on it
    // https://doc.qt.io/qt-6/qeventlooplocker.html#QEventLoopLocker
    // see also:
    // https://invent.kde.org/plasma/kwin/-/merge_requests/1267/diffs#620b62637a901cceddbd29857f98b9a970caf8f0_149_164
    a.setQuitLockEnabled(false); // I want to keep QEventLoop running

    qDebug() << "hi, testing KIO launching desktop file";

    const auto desktopName = QStringLiteral("org.kde.kwrite");
    const KService::Ptr appService = KService::serviceByDesktopName(desktopName);
    if (!appService) {
        qWarning() << "Could not find" << desktopName;
    }

    // KIO::ApplicationLauncherJob job(appService);
    // job.start();
    // leads to crash: double free or corruption (out)

    auto job = new KIO::ApplicationLauncherJob(appService, &a);
    job->start();

    return a.exec();
}
